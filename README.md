<p align="center">
  <a href="https://github.com/aluisribeiro/petsaojoao">
    <img src="logo.png" alt="Logo" width="384" height="256">
  </a>

  <h1 align="center">Pet São João</h1>

  <p align="center">
    <a href="https://github.com/aluisribeiro/petsaojoao">Reportar um Bug</a>
    ·
    <a href="https://github.com/aluisribeiro/petsaojoao">Solicitar Funcionalidade</a>
  </p>
</p>

<!-- TABLE OF CONTENTS -->
## Tabela de Conteúdo

- [Tabela de Conteúdo](#tabela-de-conte%c3%bado)
- [Sobre o Projeto](#sobre-o-projeto)
  - [Feito Com](#feito-com)
- [Começando](#come%c3%a7ando)
  - [Pré-requisitos](#pr%c3%a9-requisitos)
  - [Instalação](#instala%c3%a7%c3%a3o)
- [Contribuição](#contribui%c3%a7%c3%a3o)
- [Licença](#licen%c3%a7a)
- [Contato](#contato)

<!-- ABOUT THE PROJECT -->
## Sobre o Projeto

(Aqui vai uma descrição sobre o projeto)

### Feito Com

Abaixo podem ser encontradas as Tecnologias, Frameworks e Bibliotecas utilizadas no projeto, tanto no Front-end quando no Back-end.

<!-- GETTING STARTED -->
## Começando

Para ser possível executar uma cópia do projeto localmente é necessário seguir uma série de passos, que estão descritos abaixo:

### Pré-requisitos

Antes mesmo de ir para as configurações da aplicação, há algumas coisas que são necessárias ter configuradas em sua máquina, abaixo segue uma lista do que é necessário:

### Instalação

<!-- CONTRIBUTING -->
## Contribuição

Contribuições são o que fazem a comunidade open source um lugar incrível para aprender, inspirar e criar. Qualquer contribuição que você fizer será **muito apreciada**.

1. Faça um Fork do projeto
2. Crie uma Branch para sua Feature (`git checkout -b feature/FeatureIncrivel`)
3. Adicione suas mudanças (`git add .`)
4. Comite suas mudanças (`git commit -m 'Adicionando uma Feature incrível!`)
5. Faça o Push da Branch (`git push origin feature/FeatureIncrivel`)
6. Abra uma Pull Request

<!-- LICENSE -->
## Licença

Distribuído sob a licença MIT. Veja `LICENSE` para mais informações.

<!-- CONTACT -->
## Contato

Anderson Ribeiro - [Github](https://github.com/aluisribeiro) - **?**

Claudio Orlandi - [Github](https://github.com/csorlandi) - **cso.junior1996@gmail.com**

Otto Spreng - [Github](https://github.com/sprengzor) - **?**


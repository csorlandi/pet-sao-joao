import faker from 'faker';
import { factory } from 'factory-girl';
import User from '../../src/app/models/User';

factory.define('PendingUser', User, {
  name: faker.name.findName(),
  email: faker.internet.email(),
  provider: faker.random.word(),
  status: 'pending',
  phone_number: faker.phone.phoneNumber,
  avatar_url: faker.internet.url,
});

factory.define('User', User, {
  name: faker.name.findName(),
  email: faker.internet.email(),
  provider: faker.random.word(),
  status: faker.random.word(),
  phone_number: faker.phone.phoneNumber,
  avatar_url: faker.internet.url,
});

export default factory;

import request from 'supertest';
import app from '../../../src/app';

import factory from '../../util/factories';
import truncate from '../../util/truncate';

describe('SessionController', () => {
  beforeEach(async () => {
    await truncate();
  });

  it('should not be able to create a new session token if user status is pending', async () => {
    // const user = await factory.create('User', {
    //   status: 'pending',
    // });
    const user = await factory.attrs('PendingUser');
    console.log(user);

    await request(app)
      .post('/users')
      .send(user);

    const response = await request(app)
      .post('/sessions')
      .send({ email: user.email });

    expect(response.status).toBe(403);
  });

  // it('should be able to create a new session token if user status is not pending', async () => {
  //   const user = await factory.attrs('User');


  //   expect();
  // });
});

import User from '../models/User';

class UserController {
  async show(req, res) {
    const { email } = req.params;

    const user = await User.findOne({ where: { email } });

    return res.json(user);
  }

  async store(req, res) {
    const userExists = await User.findOne({
      where: { email: req.body.email },
    });

    if (userExists) {
      return res.status(400).json({ error: 'User already exists!' });
    }

    const { email } = await User.create({ ...req.body, status: 'pending' });

    return res.status(201).json({ email });
  }
}

export default new UserController();

import jwt from 'jsonwebtoken';

import User from '../models/User';
import authConfig from '../../config/auth';

class SessionController {
  async store(req, res) {
    const { email } = req.body;

    const user = await User.findOne({ where: { email } });

    if (user.status === 'pending') {
      return res.status(403).json({ status: user.status });
    }

    const { id, name, status, avatar_url } = user;

    return res.json({
      user: { id, name, email, status, avatar_url },
      token: jwt.sign({ id }, authConfig.secret, {
        expiresIn: authConfig.expiresIn,
      }),
    });
  }
}

export default new SessionController();

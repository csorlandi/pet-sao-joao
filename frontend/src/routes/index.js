import React from 'react';
import { Switch } from 'react-router-dom';
import Route from './Route';

import SignIn from '~/pages/SignIn';
import PhoneRequest from '~/pages/PhoneRequest';
import Warning from '~/pages/Warning';

import Dashboard from '~/pages/Dashboard';

import PendingApprovals from '~/pages/PendingApprovals';

export default function Routes() {
  return (
    <Switch>
      <Route path="/" exact component={SignIn} />
      <Route path="/cadastrar-telefone" exact component={PhoneRequest} />
      <Route path="/aviso" exact component={Warning} />

      <Route path="/dashboard" exact component={Dashboard} isPrivate />
      <Route
        path="/dashboard/usuarios/aprovacoes-pendentes"
        exact
        component={PendingApprovals}
        isPrivate
      />

      <Route
        path="/dashboard/usuarios/todos"
        exact
        component={Dashboard}
        isPrivate
      />

      <Route
        path="/dashboard/usuarios/cadastrar"
        exact
        component={Dashboard}
        isPrivate
      />
      {/*
      <Route path="/profile" component={Profile} isPrivate /> */}
    </Switch>
  );
}

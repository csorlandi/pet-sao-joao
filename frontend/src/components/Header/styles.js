import styled from 'styled-components';

export const Container = styled.header`
  background: #004884;
  padding: 0 16px;
  display: flex;
  flex-direction: row;
  align-items: center;

  svg {
    color: #ebf6ff;
    font-size: 16px;
  }
`;

export const BreadcumbContainer = styled.div`
  margin-left: 32px;
  display: flex;
  align-items: center;

  svg,
  span {
    color: #ebf6ff;
    font-size: 14px;
  }

  svg {
    margin: 0 4px;
  }
`;

export const Content = styled.aside`
  flex: 1;
  height: 64px;
  max-width: 1366px;
  margin: 0 auto;
  display: flex;
  justify-content: flex-end;
  align-items: center;
  nav {
    display: flex;
    align-items: center;
    img {
      margin-right: 20px;
      padding-right: 20px;
    }
    a {
      font-weight: bold;
      color: #ebf6ff;
    }
  }
  aside {
    display: flex;
    align-items: center;
  }
`;

export const Profile = styled.div`
  display: flex;
  margin-left: 20px;
  padding-left: 20px;
  border-left: 1px solid #ebf6ff;
  div {
    text-align: right;
    margin-right: 10px;
    strong {
      display: block;
      color: #ebf6ff;
    }
    button {
      margin: 2px 0 0;
      color: rgba(235, 246, 255, 0.6);
      font-size: 12px;
      border: 0;
      padding: 0;
      font-family: inherit;
      background-color: transparent;
    }
  }
  img {
    width: 32px;
    height: 32px;
    border-radius: 50%;
  }
`;

import React from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { FaBars } from 'react-icons/fa';

import Notifications from '~/components/Notifications';

import { signOut } from '~/store/modules/auth/actions';

import { Container, Content, Profile } from './styles';

export default function Header() {
  const dispatch = useDispatch();
  const user = useSelector(state => state.user.profile);
  console.tron.log(user);

  function handleSignOut() {
    dispatch(signOut());
  }

  return (
    <Container>
      <FaBars />
      {/* <BreadcumbContainer>
        <FaHome />
        <FaChevronRight />
        <span>Usuários</span>
        <FaChevronRight />
        <span>Lista de Usuários</span>
      </BreadcumbContainer> */}
      <Content>
        <Notifications />
        <Profile>
          <div>
            <strong>{user.name}</strong>
            <button type="button" onClick={handleSignOut}>
              Sair
            </button>
          </div>
          <img src={user.avatar_url} alt={user.name} />
        </Profile>
      </Content>
    </Container>
  );
}

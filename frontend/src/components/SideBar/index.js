import React from 'react';

import { Link } from 'react-router-dom';

import { FaHome, FaUser, FaPaw, FaExclamationTriangle } from 'react-icons/fa';

import SideBarMenu from './components/SideBarMenu';

import { Container, LogoContainer, ListContainer } from './styles';

import logo from '~/assets/logo_horizontal.png';

export default function LeftBar() {
  return (
    <Container>
      <LogoContainer>
        <Link to="/dashboard">
          <img src={logo} alt="Logo" />
        </Link>
      </LogoContainer>
      <ListContainer>
        <SideBarMenu>
          <SideBarMenu.Item icon={<FaHome />} title="Home" to="/dashboard" />
          <SideBarMenu.Item
            icon={<FaExclamationTriangle />}
            title="Aprovações Pendentes"
            to="/dashboard/usuarios/aprovacoes-pendentes"
          />
          <SideBarMenu.Item icon={<FaUser />} title="Usuários">
            <SideBarMenu.Item
              title="Listar Todos"
              to="/dashboard/usuarios/todos"
            />
            <SideBarMenu.Item
              title="Cadastrar Novo"
              to="/dashboard/usuarios/cadastrar"
            />
          </SideBarMenu.Item>
          <SideBarMenu.Item icon={<FaPaw />} title="Animais">
            <SideBarMenu.Item title="Listar Todos" to="/dashboard/pets/todos" />
            <SideBarMenu.Item
              title="Cadastrar Novo"
              to="/dashboard/pets/cadastrar"
            />
          </SideBarMenu.Item>
        </SideBarMenu>
      </ListContainer>
    </Container>
  );
}

import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import uuid from 'uuid/v4';

import { SideBarContainer } from './styles';

function SideBarMenu({ children, location }) {
  const [menuItems, setMenuItems] = useState([]);

  useEffect(() => {
    function addItems() {
      const aux = [];
      for (let i = 0; i < children.length; i += 1) {
        const id = uuid();

        aux.push({
          id,
          open: false,
          active: false,
          children: [],
        });

        if (children[i].props.children) {
          for (let j = 0; j < children[i].props.children.length; j += 1) {
            const childrenId = uuid();

            aux[i].children.push({
              id: childrenId,
              open: false,
              active: false,
            });
          }
        }
      }
      setMenuItems(aux);
    }

    addItems();
  }, []); //eslint-disable-line

  // useEffect(() => {
  //   function findIndex(items) {
  //     let indexObject = null;

  //     menuItems.forEach((_parentItem, parentIndex) => {
  //       const { pathname } = location;

  //       const noTailSlashLocation =
  //         pathname[pathname.length - 1] === '/' && pathname.length > 1
  //           ? pathname.replace(/\/$/, '')
  //           : pathname;

  //       if (items[parentIndex].props.to === noTailSlashLocation) {
  //         indexObject = {
  //           parentIndex: null,
  //           childIndex: parentIndex,
  //         };
  //       } else if (menuItems[parentIndex].children.length) {
  //         menuItems[parentIndex].children.forEach((_childItem, childIndex) => {
  //           if (
  //             items[parentIndex].props.children[childIndex].props.to ===
  //             noTailSlashLocation
  //           ) {
  //             indexObject = {
  //               parentIndex,
  //               childIndex,
  //             };
  //           }
  //         });
  //       }
  //     });

  //     return indexObject;
  //   }

  //   function setActiveItem() {
  //     const indexObject = findIndex(children);

  //     if (indexObject) {
  //       if (
  //         indexObject.childIndex !== null &&
  //         indexObject.parentIndex === null
  //       ) {
  //         if (!menuItems[indexObject.childIndex].active) {
  //           setMenuItems(
  //             menuItems.map((parentItem, parentIndex) => {
  //               if (parentIndex === indexObject.childIndex) {
  //                 return {
  //                   ...parentItem,
  //                   active: true,
  //                 };
  //               }

  //               return parentItem;
  //             })
  //           );
  //         }
  //       } else if (
  //         indexObject.childIndex !== null &&
  //         indexObject.parentIndex !== null
  //       ) {
  //         if (
  //           !menuItems[indexObject.parentIndex].children[indexObject.childIndex]
  //             .active
  //         ) {
  //           setMenuItems(
  //             menuItems.map((parentItem, parentIndex) => {
  //               if (parentIndex === indexObject.parentIndex) {
  //                 return {
  //                   ...parentItem,
  //                   open: true,
  //                   children: parentItem.children.map(
  //                     (childItem, childIndex) => {
  //                       if (childIndex === indexObject.childIndex) {
  //                         return {
  //                           ...childItem,
  //                           active: true,
  //                         };
  //                       }

  //                       return childItem;
  //                     }
  //                   ),
  //                 };
  //               }

  //               return parentItem;
  //             })
  //           );
  //         }
  //       }
  //     }
  //   }

  //   setActiveItem();
  // }, [children, location, menuItems]);

  function updateItem(id, data) {
    const menuItemsIndex = menuItems.findIndex(findItem => findItem.id === id);

    setMenuItems(
      menuItems.map((item, index) => {
        if (index === menuItemsIndex) {
          return {
            ...item,
            ...data,
          };
        }
        return item;
      })
    );
  }

  return (
    <SideBarContainer>
      {menuItems.length &&
        children.map((child, index) => (
          <child.type
            {...child.props}
            key={menuItems[index].id}
            item={menuItems[index]}
            updateItem={updateItem}
            currentUrl={location.pathname}
          />
        ))}
    </SideBarContainer>
  );
}

SideBarMenu.propTypes = {
  children: PropTypes.node,
  location: PropTypes.shape({
    pathname: PropTypes.string,
  }).isRequired,
};

SideBarMenu.defaultProps = {
  children: null,
};

export default withRouter(SideBarMenu);

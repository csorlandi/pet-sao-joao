import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

export default function SideBarMenuItemLink({ to, children, onToggle }) {
  if (to) {
    return <Link to={to}>{children}</Link>;
  }
  return (
    <button type="button" onClick={() => onToggle()}>
      {children}
    </button>
  );
}

SideBarMenuItemLink.propTypes = {
  to: PropTypes.string,
  onToggle: PropTypes.func.isRequired,
  children: PropTypes.node.isRequired,
};

SideBarMenuItemLink.defaultProps = {
  to: null,
};

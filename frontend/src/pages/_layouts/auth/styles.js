import styled from 'styled-components';

export const Wrapper = styled.div`
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  background-color: #fafafa;
`;

export const Content = styled.div`
  width: 100%;
  max-width: 360px;
  text-align: center;
  background-color: #fff;
  border-radius: 8px;
  padding: 48px;
  box-shadow: 0 0 20px 0 rgba(0, 101, 184, 0.2),
    0 5px 5px 0 rgba(0, 101, 184, 0.2);

  img {
    margin-bottom: 32px;
  }

  h1 {
    font-size: 24px;
    color: #232e3b;
    margin-bottom: 8px;
  }

  p {
    font-size: 16px;
    margin-bottom: 16px;
    color: #9fa9b5;
  }

  form {
    display: flex;
    flex-direction: column;

    input {
      background: #f2f2f2;
      font-size: 16px;
      border: 0;
      border-radius: 4px;
      height: 48px;
      padding: 0 16px;
      color: #004884;
      &::placeholder {
        color: #0065b8;
      }
    }
    span {
      color: #fb6f91;
      align-self: center;
      font-weight: bold;
      margin-top: 8px;
    }

    button {
      margin: 8px 0 0;
      height: 44px;
      background: #004884;
      font-weight: bold;
      color: #fff;
      border: 0;
      border-radius: 4px;
      font-size: 16px;
      transition: background 0.2s;

      &:first-child {
        margin: 0;
      }

      &:hover {
        background: #0065b8;
      }
    }
  }

  button {
    margin: 16px 0 0;
    color: #004884;
    font-size: 12px;
    border: 0;
    padding: 0;
    font-family: inherit;
  }
`;

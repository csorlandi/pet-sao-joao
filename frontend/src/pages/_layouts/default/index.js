import React from 'react';
import PropTypes from 'prop-types';

import Header from '~/components/Header';
import SideBar from '~/components/SideBar';

import {
  Container,
  Content,
  RightContent,
  MainContentContainer,
} from './styles';

export default function DefaultLayout({ children }) {
  return (
    <Container>
      <Content>
        <SideBar />
        <RightContent>
          <Header />
          <MainContentContainer>{children}</MainContentContainer>
        </RightContent>
      </Content>
    </Container>
  );
}

DefaultLayout.propTypes = {
  children: PropTypes.element.isRequired,
};

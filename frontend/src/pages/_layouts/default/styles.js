import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
  height: 100%;
  flex: 1;
  flex-direction: row;
  background-color: #e2e3eb;
  justify-content: center;
`;

export const Content = styled.div`
  display: flex;
  flex: 1;
  max-width: 1366px;
`;

export const RightContent = styled.div`
  flex: 1;
  height: 100%;
  margin: 0 auto;
  display: flex;
  flex-direction: column;
`;

export const MainContentContainer = styled.div`
  display: flex;
  flex: 1;
`;

import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
  flex: 1;
  flex-direction: column;
  padding: 24px;
  background-color: #dfdfe6;
`;

export const ContentContainer = styled.div`
  display: flex;
  flex: 1;
  padding: 8px;
  border-radius: 8px;
  background-color: #fff;
`;

import React from 'react';

import { Container, ContentContainer } from './styles';

export default function Dashboard() {
  return (
    <Container>
      <ContentContainer>
        <h1>Home</h1>
      </ContentContainer>
    </Container>
  );
}

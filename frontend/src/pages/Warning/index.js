import React, { useEffect } from 'react';
import PropTypes from 'prop-types';

import history from '~/services/history';

import logo from '~/assets/logo_small.png';

export default function Warning({ location }) {
  useEffect(() => {
    if (!location.state) {
      history.push('/');
    }
  }, [location.state]);

  function handleCancel() {
    history.push('/');
  }

  return (
    <>
      <img src={logo} alt="Pet São João" />
      <h1>Você já está cadastrado!</h1>
      <p>
        Aguarde um administrador aprovar seu cadastro para ter acesso ao Painel
        de Controle.
      </p>
      <button type="button" onClick={handleCancel}>
        Voltar
      </button>
    </>
  );
}

Warning.propTypes = {
  location: PropTypes.shape({
    state: PropTypes.shape({
      origin: PropTypes.string,
    }),
  }),
};

Warning.defaultProps = {
  location: {
    state: undefined,
  },
};

import styled from 'styled-components';

import DataTable from 'react-data-table-component';

export const Container = styled.div`
  display: flex;
  flex: 1;
  flex-direction: column;
  padding: 24px;
  background-color: #dfdfe6;
`;

export const DataTableContainer = styled.div`
  display: flex;
  flex: 1;
  padding: 8px;
  border-radius: 8px;
  background-color: #fff;
`;

export const CustomDataTable = styled(DataTable).attrs({
  highlightOnHover: true,
  noDataComponent: 'Nenhuma aprovação pendente',
  paginationComponentOptions: {
    noRowsPerPage: true,
  },
})`
  div {
    height: auto;
  }
`;

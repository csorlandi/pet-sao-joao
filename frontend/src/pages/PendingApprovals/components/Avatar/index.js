import React from 'react';

import { Image } from './styles';

export default function Avatar(props) {
  return (
    <Image {...props} />
  );
}

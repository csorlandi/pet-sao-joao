import styled from 'styled-components';

export const Container = styled.button`
  border: 0;
  padding: 4px 8px;
  background-color: #ff4242;
  color: #fff;
  font-weight: bold;
  border-radius: 4px;

  &:hover {
    background-color: #e63c3c;
  }
`;

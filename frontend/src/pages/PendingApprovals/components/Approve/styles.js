import styled from 'styled-components';

export const Container = styled.button`
  border: 0;
  padding: 4px 8px;
  background-color: #06b49a;
  color: #fff;
  font-weight: bold;
  border-radius: 4px;

  &:hover {
    background-color: #059c85;
  }
`;

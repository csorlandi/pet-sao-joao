import React from 'react';

import Avatar from './components/Avatar';
import Approve from './components/Approve';
import Reprove from './components/Reprove';

import { Container, DataTableContainer, CustomDataTable } from './styles';

import data from './data.json';

const columns = [
  {
    name: 'Avatar',
    cell: row => <Avatar alt={row.title} src="https://picsum.photos/40" />,
    grow: 0,
  },
  {
    name: 'Nome',
    selector: 'title',
  },
  {
    name: 'Email',
    selector: 'director',
  },
  {
    name: 'Provedor',
    selector: 'year',
  },
  {
    button: true,
    cell: row => <Approve id={row.id} />,
  },
  {
    button: true,
    cell: row => <Reprove id={row.id} />,
  },
];

export default function PendingApprovals() {
  return (
    <Container>
      <DataTableContainer>
        <CustomDataTable
          title="Aprovações Pendentes"
          columns={columns}
          data={data}
          pagination
        />
      </DataTableContainer>
    </Container>
  );
}

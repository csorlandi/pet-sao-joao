import React from 'react';
import PropTypes from 'prop-types';

import { useDispatch } from 'react-redux';

import { signInWithFirebaseRequest } from '~/store/modules/auth/actions';

import { withFirebase } from '~/components/Firebase';

import logo from '~/assets/logo_small.png';

function SignIn({ firebase }) {
  const dispatch = useDispatch();

  async function handleSubmit(provider) {
    dispatch(signInWithFirebaseRequest(provider, firebase));
  }

  return (
    <>
      <img src={logo} alt="Pet São João" />
      <form>
        <button type="button" onClick={() => handleSubmit('facebook')}>
          Entrar com Facebook
        </button>
        <button type="button" onClick={() => handleSubmit('google')}>
          Entrar com Google
        </button>
      </form>
    </>
  );
}

SignIn.propTypes = {
  firebase: PropTypes.shape().isRequired,
};

export default withFirebase(SignIn);

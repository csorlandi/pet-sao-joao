import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { useDispatch } from 'react-redux';

import { Form, Input } from '@rocketseat/unform';
import * as Yup from 'yup';

import { signUpRequest } from '~/store/modules/auth/actions';

import history from '~/services/history';

import logo from '~/assets/logo_small.png';

const schema = Yup.object().shape({
  phone: Yup.string().required('O Telefone é obrigatório'),
});

export default function PhoneRequest({ location }) {
  const dispatch = useDispatch();

  useEffect(() => {
    if (!location.state) {
      history.push('/');
    }
  }, [location.state]);

  function handlePhoneSubmit({ phone }) {
    console.tron.log('location', location.state);
    const updatedData = { ...location.state, phone_number: phone };

    console.tron.log('updated data', updatedData);

    dispatch(signUpRequest(updatedData));
  }

  function handleCancel() {
    history.goBack();
  }

  return (
    <>
      <img src={logo} alt="Pet São João" />
      <h1>Falta só mais um passo!</h1>
      <p>
        Precisamos do seu Telefone para te avisar de qualquer alteração no seu
        cadastro.
      </p>
      <Form schema={schema} onSubmit={handlePhoneSubmit}>
        <Input name="phone" type="text" placeholder="Digite seu Telefone" />

        <button type="submit">Acessar</button>
      </Form>
      <button type="button" onClick={handleCancel}>
        Cancelar
      </button>
    </>
  );
}

PhoneRequest.propTypes = {
  location: PropTypes.shape({
    state: PropTypes.shape({
      name: PropTypes.string,
      email: PropTypes.string,
      avatar_url: PropTypes.string,
      provider: PropTypes.string,
      phone_number: PropTypes.oneOfType([() => null, PropTypes.string]),
    }),
  }),
};

PhoneRequest.defaultProps = {
  location: {
    state: undefined,
  },
};

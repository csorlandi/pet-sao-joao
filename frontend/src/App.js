import React from 'react';

import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';

import { ToastContainer } from 'react-toastify';
import { Router } from 'react-router-dom';

import '~/config/ReactotronConfig';

import Firebase, { FirebaseContext } from '~/components/Firebase';

import Routes from '~/routes';
import history from '~/services/history';

import { store, persistor } from '~/store';

import GlobalStyle from '~/styles/global';

function App() {
  return (
    <>
      <FirebaseContext.Provider value={new Firebase()}>
        <Provider store={store}>
          <PersistGate persistor={persistor}>
            <Router history={history}>
              <Routes />
              <GlobalStyle />
              <ToastContainer autoClose={3000} />
            </Router>
          </PersistGate>
        </Provider>
      </FirebaseContext.Provider>
    </>
  );
}

export default App;

import { takeLatest, call, put, all } from 'redux-saga/effects';

import { toast } from 'react-toastify';

import history from '~/services/history';
import api from '~/services/api';

import {
  signInRequest,
  signInSuccess,
  signUpRequest,
  signFailure,
} from './actions';

export function* signInWithFirebase({ payload }) {
  try {
    const { provider, firebase } = payload;

    let firebaseData = null;

    if (provider === 'google') {
      firebaseData = yield call(firebase.doSignInWithGoogle);
    } else {
      firebaseData = yield call(firebase.doSignInWithFacebook);
    }

    const userExists = yield call(api.get, `users/${firebaseData.user.email}`);

    if (!userExists.data) {
      const data = {
        name: firebaseData.user.displayName,
        email: firebaseData.user.email,
        phone_number: firebaseData.user.phoneNumber,
        provider,
        avatar_url: firebaseData.user.photoURL,
      };

      if (!firebaseData.user.phoneNumber) {
        history.push('/cadastrar-telefone', data);
        return;
      }

      yield put(signUpRequest(data));
      return;
    }

    yield put(signInRequest({ email: firebaseData.user.email }));
  } catch (error) {
    console.tron.log('signInWithFirebase error', error);
    yield put(signFailure());
  }
}

export function* signIn({ payload: { data } }) {
  try {
    const response = yield call(api.post, 'sessions', data);

    const { user, token } = response.data;

    api.defaults.headers.Authorization = `Bearer ${token}`;

    yield put(signInSuccess(token, user));

    history.push('/dashboard');
  } catch (error) {
    if (error.response.status === 403) {
      history.push('/aviso', { origin: 'signIn' });
    } else {
      toast.error('Falha na Autenticação!');
    }
    yield put(signFailure());
  }
}

export function* signUp({ payload: { data } }) {
  try {
    const response = yield call(api.post, 'users', data);

    if (response.data.error) {
      toast.error('Usuário já cadastrado!');
      return;
    }

    yield put(signInRequest({ email: response.data.email }));
  } catch (error) {
    console.tron.log(error);
    toast.error('Falha no cadastro, verifique seus dados!');
    yield put(signFailure());
  }
}

export function signOut() {
  history.push('/');
}

export function setToken({ payload }) {
  if (!payload) return;

  const { token } = payload.auth;

  if (token) {
    api.defaults.headers.Authorization = `Bearer ${token}`;
  }
}

export default all([
  takeLatest('persist/REHYDRATE', setToken),
  takeLatest('@auth/SIGN_IN_WITH_FIREBASE_REQUEST', signInWithFirebase),
  takeLatest('@auth/SIGN_IN_REQUEST', signIn),
  takeLatest('@auth/SIGN_UP_REQUEST', signUp),
  takeLatest('@auth/SIGN_OUT', signOut),
]);
